<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>q1</title>

    <!-- Bootstrap -->
    <link href="bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<form action="{{route('store')}}" method="post" onsubmit="submitDataByAjax(); return false">
    @csrf
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <h1>add product</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <label for="product"> product name</label>
                <input type="text" class="form-control" placeholder="product name" name="product" id="product">
            </div>

        </div>
        <div class="row">
            <div class="col-md-3">
                <label for='quantity'>quantity</label>
                <input type="text" class="form-control" placeholder="Quantity in stock" name="quantity" id="quantity">
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <label for='price'>price</label>
                <input type="text" class="form-control" placeholder="Price per item." name="price" id="price">
            </div>
        </div>
        <div class="row" style="margin-top: 20px">
            <div class="col-md-3">
                <input type="submit" class="btn btn-success btn-lg " value="add">
            </div>
        </div>
    </div>
</form>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>
<script>
    function submitDataByAjax(){

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                alert('file created');
            }
        };
        xhttp.open("POST", "/store", true);
        var  product = document.getElementById("product").value;
        var quantity= document.getElementById("quantity").value;
        var price= document.getElementById("price").value;
        xhttp.send('product='+product+'&quantity='+quantity+'&price='+price);

    }
</script>