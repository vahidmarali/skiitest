<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    public function create(){
        return view('create');
    }
    public function store(Request $request){

        $product =$request['product'];
        $quantity=$request['quantity'];
        $price=$request['price'];
        $array=compact('product','quantity','price');
        $path = Storage::put('file.json', json_encode($array));
        dd($path);
        $data_file = fopen("data.json", "w") or die("Unable to open file!");
        fwrite($data_file, json_encode($array));
        echo 'Done';

    }
}
